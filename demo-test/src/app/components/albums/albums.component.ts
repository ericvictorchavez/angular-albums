import { Component, OnInit } from '@angular/core';
import {Albums} from "../models/Albums";
import {AlbumsService} from "../../services/albums.service";
@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class albumsComponent implements OnInit {


  albums: Albums[];



  constructor(private dataService: AlbumsService) {
  }

  ngOnInit(): void {
    this.dataService.getAlbums().subscribe(albums =>{
      this.albums =albums
    })

  }


}

