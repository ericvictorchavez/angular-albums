export interface Albums {
  title: string,
  artist: string,
  songs: string[],
  favorite: string,
  year: string,
  genre: string,
  units: number
  cover: string,
}
