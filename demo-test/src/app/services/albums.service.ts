import { Injectable } from '@angular/core';
import {Albums} from "../components/models/Albums";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  albums: Albums[];
  data:Observable<any>

  constructor() {
    this.albums = [
    {
      title: 'Goodbye and Good Riddance',
      artist: 'Juice Wrld',
      songs: ['Intro', 'All Girls Are the Same','Lucid Dreams','Wasted','Armed and Dangerous','ect'],
      favorite: 'Lucid Dreams',
      year: '05/23/2018',
      genre: 'Rap',
      units: +1000000,
      cover: "/assets/img/Jucie.jpg" ,
    },
    {
      title: '?',
      artist: 'XXXTENTACION',
      songs: ['SAD!','Moonlight','Floor 555','NUMB','ALONE,PART 3','changes','Hope','ect'],
      favorite: 'SAD!',
      year: '03/16/2018',
      genre: 'Rap',
      units: 35000000,
      cover: '/assets/img/xxx.jpeg',
    },
    {
      title: 'The Thrill of it All',
      artist: 'Sam Smith',
      songs: ['Too Good at Goodbyes','Say it First','One Last Song','Midnight','Burning','ect'],
      favorite: 'Too Good at Goodbyes',
      year: '11/03/2017',
      genre: 'Pop/Soul music',
      units: 400000,
      cover: "/assets/img/sam smith.jpeg",
    },
    {
      title: 'A Night at the Opera',
      artist: 'Queen',
      songs: ['Love of My Life','Good Company','Bohemian Rhapsody','Lazing on a Sunday Afternoon','ect'],
      favorite: 'Bohemian Rhapsody',
      year: '11/21/1975',
      genre: 'Rock',
      units: 6000000,
      cover: "/assets/img/aNightAtTheOpera.jpeg",
    },
    {
      title: 'Stoney',
      artist: 'Post Malone',
      songs: ['Fall Apart', 'Feel','Congratulations','White Iverson','Broken Whiskey Glass','ect'],
      favorite: 'White Iverson',
      year: '12/09/2016',
      genre: 'Hip Hop/Pop/Rap rock music',
      units: 1000000,
      cover: "/assets/img/Stoney.jpeg",
    }
  ];
  }
  getAlbums(): Observable<Albums[]> {
    console.log('Fetching users from service');
    return of(this.albums);
  }
  // Create a method that adds a new user to the array
  addAlbums(albums: Albums){
    console.log('Added user from service');
    this.albums.unshift(albums);
  }
}
