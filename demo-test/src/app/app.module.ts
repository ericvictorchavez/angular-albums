import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ParticlesModule} from 'angular-particle';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { albumsComponent} from "./components/albums/albums.component";

@NgModule({
  declarations: [
    AppComponent,
    albumsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ParticlesModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
